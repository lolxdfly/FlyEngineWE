package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;

public final class Menu extends JMenuBar {
	
	public Menu()
	{
		///////////////////////////////////////File///////////////////////////////////////
		final JMenu pFile = new JMenu("File");
		/*****************New*****************/
		final JMenuItem bNew = new JMenuItem("New TileSet");
		bNew.addActionListener(
				new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {
						final JFileChooser fc = new JFileChooser();
						fc.setFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
				        if(fc.showDialog(null, "Open TileTexture") == JFileChooser.APPROVE_OPTION)
				        {
				        	//reset
				        	if(MainFrame.worldtilepanel != null)
				        		MainFrame.mainframe.remove(MainFrame.worldtilepanel);
				        	if(MainFrame.tilepanel != null)
				        		MainFrame.mainframe.remove(MainFrame.tilepanel);
				        	
				        	//open toolbar
				        	MainFrame.toolpanel = new TileToolPanel();
				        	MainFrame.mainframe.add(MainFrame.toolpanel, BorderLayout.PAGE_START);
				        	
				        	//open tilepanel
				        	MainFrame.tilepanel = new TilePanel();
				        	MainFrame.mainframe.add(MainFrame.tilepanel, BorderLayout.LINE_START);
				        	
				        	//open worldtilepanel
				        	BufferedImage tileimg = null;
							try {
								tileimg = ImageIO.read(fc.getSelectedFile());
							} catch (IOException e1) {
								e1.printStackTrace();
							}
				        	
							MainFrame.worldtilepanel = new WorldTilePanel(tileimg);
							MainFrame.mainframe.add(MainFrame.worldtilepanel, BorderLayout.CENTER);
				        	
				        	//set tile image
							MainFrame.tilepanel.setTileImage(tileimg);
				        }
					}
				});
		pFile.add(bNew);
		/*****************Load TileSet*****************/
		final JMenuItem bLoadTS = new JMenuItem("Open TileSet");
		bLoadTS.addActionListener(
				new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						final JFileChooser fc = new JFileChooser();
						final FileNameExtensionFilter filter = new FileNameExtensionFilter("TileSet", "tileset", "tileset");
						fc.setFileFilter(filter);
				        if(fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				        {
				        	//reset
				        	if(MainFrame.worldtilepanel != null)
				        		MainFrame.mainframe.remove(MainFrame.worldtilepanel);
				        	if(MainFrame.tilepanel != null)
				        		MainFrame.mainframe.remove(MainFrame.tilepanel);

				        	//open toolbar
				        	MainFrame.toolpanel = new TileToolPanel();
				        	MainFrame.mainframe.add(MainFrame.toolpanel, BorderLayout.PAGE_START);
				    		
				        	//open and load tilepanel
				        	MainFrame.tilepanel = new TilePanel();
				        	MainFrame.mainframe.add(MainFrame.tilepanel, BorderLayout.LINE_START);
				        	MainFrame.tilepanel.load(fc.getSelectedFile());
				        	
				        	//open worldtilepanel
				        	MainFrame.worldtilepanel = new WorldTilePanel(MainFrame.tilepanel.getTileImage());
				        	MainFrame.mainframe.add(MainFrame.worldtilepanel, BorderLayout.CENTER);
				        }
					}
				});
		pFile.add(bLoadTS);
		/*****************Open*****************/
		final JMenuItem bOpen = new JMenuItem("Open WorldProject");
		bOpen.addActionListener(
				new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						final JFileChooser fcwld = new JFileChooser();
						final FileNameExtensionFilter filterwld = new FileNameExtensionFilter("WorldProject", "wld", "wld");
						fcwld.setFileFilter(filterwld);

						final JFileChooser fctile = new JFileChooser();
						final FileNameExtensionFilter filtertile = new FileNameExtensionFilter("TileSet", "tileset", "tileset");
						fctile.setFileFilter(filtertile);
						
				        if(fcwld.showOpenDialog(null) == JFileChooser.APPROVE_OPTION && fctile.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				        {
				        	//reset
				        	if(MainFrame.worldtilepanel != null)
				        		MainFrame.mainframe.remove(MainFrame.worldtilepanel);
				        	if(MainFrame.tilepanel != null)
				        		MainFrame.mainframe.remove(MainFrame.tilepanel);				        	
				        	if(MainFrame.toolpanel != null)
				        		MainFrame.mainframe.remove(MainFrame.toolpanel);
		
				        	//open toolbar
				        	MainFrame.toolpanel = new WorldToolPanel();
				        	MainFrame.mainframe.add(MainFrame.toolpanel, BorderLayout.PAGE_START);

				        	//open worldpanel
				        	MainFrame.worldpanel = new WorldPanel();
				        	MainFrame.mainframe.add(MainFrame.worldpanel, BorderLayout.CENTER);
				        	
				        	//open tilepanel
				        	MainFrame.tilepanel = new TilePanel();
				        	MainFrame.mainframe.add(MainFrame.tilepanel, BorderLayout.LINE_START);
				        	
				        	//load
				        	MainFrame.tilepanel.load(fctile.getSelectedFile());
				        	MainFrame.worldpanel.load(fcwld.getSelectedFile());
				        	MainFrame.mainframe.setVisible(true);
				        }
					}
				});
		pFile.add(bOpen);
		add(pFile);

		///////////////////////////////////////Help///////////////////////////////////////
		final JMenu pHelp = new JMenu("Help");
		/*****************Exit*****************/
		final JMenuItem bAbout = new JMenuItem("About");
		bAbout.addActionListener(
				new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {
						final JLabel credit = new JLabel("<html><center><font size='6'>FlyEngine - WorldEditor<br>by lolxdfly<br>Ver: 1.00</font></center></html>");			
						final JDialog pAbout = new JDialog();						
						pAbout.setTitle("About");
						pAbout.setSize(300, 150);
						pAbout.add(credit);
						pAbout.setLocationRelativeTo(null);						
						pAbout.setVisible(true);
					}
				});
		pHelp.add(bAbout);
		/*****************Exit*****************/
		final JMenuItem bExit = new JMenuItem("Exit");
		bExit.addActionListener(
				new ActionListener() {					
					@Override
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
						
					}
				});
		pHelp.add(bExit);
		add(pHelp);
	}
}
