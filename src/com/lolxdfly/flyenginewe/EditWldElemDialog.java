package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;

public final class EditWldElemDialog  extends JDialog{

	public EditWldElemDialog(final Tile t) {
		//init
        setTitle("Edit Tile");
        setModal(true);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(5, 1));
        
        //todo
        final JCheckBox cbA = new JCheckBox("A");
        cbA.setSelected((t.dwObjFlags & WorldPanel.A) != 0);
        add(cbA);
        final JCheckBox cbB = new JCheckBox("B");
        cbB.setSelected((t.dwObjFlags & WorldPanel.B) != 0);
        add(cbB);
        final JCheckBox cbC = new JCheckBox("C");
        cbC.setSelected((t.dwObjFlags & WorldPanel.C) != 0);
        add(cbC);
        
        //finish
        final JButton bFinish = new JButton("Finish");
        bFinish.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  t.dwObjFlags = 0;
        	  if(cbA.isSelected())
        		  t.dwObjFlags |= WorldPanel.A;
        	  if(cbB.isSelected())
        		  t.dwObjFlags |= WorldPanel.B;
        	  if(cbC.isSelected())
        		  t.dwObjFlags |= WorldPanel.C;
        	          	  
        	  dispose();
          }
        });
        add(bFinish);
        getRootPane().setDefaultButton(bFinish);
        
        //finish
        final JButton bDelete = new JButton("Delete");
        bDelete.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  MainFrame.worldpanel.RemoveTile(t);
        	  dispose();
          }
        });
        add(bDelete);
        
        pack();
        setVisible(true);
	}
}
