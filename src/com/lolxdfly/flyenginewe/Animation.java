package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

//bAnim(true) RID glani.width glani.height glani.dtime left top right bottom dwObjFlags
public final class Animation {

	public String RID;
	public double width;
	public double height;
	public int dtime;
	public final double pos[] = new double[4];
	public int dwObjFlags;
	
	@Override
	public final String toString() {
		return RID;
	}
	
	
}
