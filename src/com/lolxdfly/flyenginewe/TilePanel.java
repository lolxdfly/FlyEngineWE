package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;


public final class TilePanel extends JPanel {

	private BufferedImage tileimage;
	private Vector<Tile> mTiles;
	private final JList<Tile> pList;
	
	public TilePanel()
	{
		setLayout(new GridLayout(0, 1));
		setBorder(new LineBorder(Color.BLACK, 1));

		mTiles = new Vector<Tile>();
		pList = new JList<Tile>();
		pList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pList.setListData(mTiles);
		pList.setCellRenderer(new TileRenderer());
		pList.addMouseListener(new CustomMouseListener());
		pList.addMouseMotionListener(new CustomMouseMotionListener());
		
        /*Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
            public void eventDispatched(AWTEvent event) {
            	System.out.println("ed");
                if(event instanceof MouseEvent){
                    final MouseEvent e = (MouseEvent)event;
                	System.out.println("me" + e.getID());
                    if(e.getID() == MouseEvent.MOUSE_DRAGGED){
                    	System.out.println("med");
            			if(MainFrame.worldpanel != null && MainFrame.worldpanel.isShowing())
            				MainFrame.worldpanel.setDragPos((int)(e.getX() - getBounds().getWidth()), e.getY());
                    }
                }
            }
        }, AWTEvent.MOUSE_EVENT_MASK);*/

		add(pList);
		add(new JScrollPane(pList));
		setVisible(true);
	}
	
	public void setTileImage(final BufferedImage bi)
	{
		tileimage = bi;
	}
	
	public final BufferedImage getTileImage()
	{
		return tileimage;
	}
	
	public void add(final Tile t)
	{
		mTiles.add(t);
		pList.setListData(mTiles);
	}
	public void remove(final Tile t)
	{
		mTiles.remove(t);
		pList.setListData(mTiles);
	}
	
	public void reset()
	{
		mTiles.clear();
		pList.setListData(mTiles);
	}
	
	private final class CustomMouseListener extends MouseAdapter {		
		@Override
		public void mousePressed(MouseEvent e)
		{	
			if(MainFrame.worldpanel != null && MainFrame.worldpanel.isShowing())
			{
				MainFrame.worldpanel.drag = pList.getSelectedValue();
				pList.setEnabled(false);				
			}
			else if (e.getClickCount() == 2)
			{
				new EditTileDialog(pList.getSelectedValue());
			}
		}
		@Override
		public void mouseReleased(MouseEvent e) {			
			if(MainFrame.worldpanel != null && MainFrame.worldpanel.isShowing())
			{
				if(MainFrame.worldpanel.drag != null
            		&& !getBounds().contains(new Point(e.getX(), e.getY())))
				{
					MainFrame.worldpanel.drag.uv_pos[4] = e.getX() - getBounds().getWidth();
					MainFrame.worldpanel.drag.uv_pos[5] = e.getY();
					MainFrame.worldpanel.AddTile(MainFrame.worldpanel.drag);
				}
			
				MainFrame.worldpanel.drag = null;
				MainFrame.worldpanel.repaint();
			
				pList.setEnabled(true);
			}
		}
	}
	private final class CustomMouseMotionListener extends MouseMotionAdapter {		
		@Override
		public void mouseDragged(MouseEvent e) {
			if(MainFrame.worldpanel != null && MainFrame.worldpanel.isShowing())
				MainFrame.worldpanel.setDragPos((int)(e.getX() - getBounds().getWidth()), e.getY());
		}
	}
	
	public void save(File f)        	  
	{
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f)))
		{
			oos.writeObject(mTiles);
            final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            ImageIO.write(tileimage, "png", buffer);
            oos.writeInt(buffer.size());
            buffer.writeTo(oos);
	        /*for (int i = 0; i < mTiles.size(); i++)
	        {
	            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	            ImageIO.write(mTiles.get(i).tileimage, "png", buffer);
	            oos.writeInt(buffer.size());
	            buffer.writeTo(oos);
	        }*/
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public void load(File f)        	  
	{
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f)))
		{
			mTiles = (Vector<Tile>)ois.readObject();
            final int size = ois.readInt();
            final byte[] buffer = new byte[size];
            ois.readFully(buffer);
        	tileimage = ImageIO.read(new ByteArrayInputStream(buffer));
        	for(Tile tile : mTiles)
        		tile.tileimage = tileimage.getSubimage((int)tile.uv_pos[0], (int)tile.uv_pos[1], (int)(tile.uv_pos[2] - tile.uv_pos[0]), (int)(tile.uv_pos[3] - tile.uv_pos[1]));
	        /*for (int i = 0; i < mTiles.size(); i++)
	        {
	            final int size = ois.readInt();
	            final byte[] buffer = new byte[size];
	            ois.readFully(buffer);
	        	mTiles.get(i).tileimage = ImageIO.read(new ByteArrayInputStream(buffer));	        	
	        }*/
		}
		catch (IOException | ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		pList.setListData(mTiles);
	}
}
