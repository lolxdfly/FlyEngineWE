package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public final class TileRenderer extends JLabel implements ListCellRenderer<Tile>{

	@Override
    public Component getListCellRendererComponent(JList<? extends Tile> list, Tile tile, int index,
            boolean isSelected, boolean cellHasFocus) {
		
		final JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 2));
		p.add(new JLabel(new ImageIcon(tile.tileimage)));
		p.add(new JLabel(tile.toString()));		
        
        if (isSelected)
        	p.setBackground(Color.GRAY);
        else
        	p.setBackground(list.getBackground());
		
		return p;
	}
	

}
