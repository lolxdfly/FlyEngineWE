package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

public final class WorldTilePanel extends JPanel {

	public BufferedImage tile;
	public final double rect[] = new double[4];
	private final double p1[] = new double[2], p2[] = new double[2];
	private double scale = 1.0;
	private final double trans[] = new double[2];
	private final double curtrans[] = new double[2];
	
	public WorldTilePanel(final BufferedImage tileimage)
	{
		tile = tileimage;
		setBorder(new LineBorder(Color.BLACK, 1));
		MainFrame.mainframe.setSize((int)(tileimage.getWidth() * 1.5), (int)(tileimage.getHeight() * 1.5));
		addMouseListener(new CustomMouseListener());
		addMouseMotionListener(new CustomMouseMotionListener());
		addMouseWheelListener(new CustomMouseWheelListener());
		setVisible(true);
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		tile.flush();
		tile = null;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	    final Graphics2D g2d = (Graphics2D) g;
	    g2d.scale(scale, scale);
	    g2d.translate(trans[0] + curtrans[0], trans[1] + curtrans[1]);
	    
	    g2d.drawImage(tile, 0, 0, this);		
	    g2d.drawRect((int)rect[0], (int)rect[1], (int)rect[2], (int)rect[3]);
	}
	
	private final class CustomMouseListener extends MouseAdapter {		
		@Override
		public void mousePressed(MouseEvent e) {
			p1[0] = e.getX();
			p1[1] = e.getY();
		}
		@Override
		public void mouseReleased(MouseEvent e) {
	        trans[0] += curtrans[0];
	        trans[1] += curtrans[1];
	        curtrans[0] = 0;
	        curtrans[1] = 0;
			if(rect[2] == 0 || rect[3] == 0)
				return;

	        new AddTileDialog();
	        rect[0] = rect[1] = rect[2] = rect[3] = 0;
			repaint();
		}
	}
	private final class CustomMouseMotionListener extends MouseMotionAdapter {		
		@Override
		public void mouseDragged(MouseEvent e) {
			
			p2[0] = e.getX();
			p2[1] = e.getY();
			
			if(SwingUtilities.isLeftMouseButton(e))
			{				
				rect[0] = p1[0] < p2[0] ? p1[0] : p2[0];
				rect[1] = p1[1] < p2[1] ? p1[1] : p2[1];
				rect[2] = Math.abs(p1[0] - p2[0]);
				rect[3] = Math.abs(p1[1] - p2[1]);
					
				//translate/scale back
				rect[0] -= trans[0] * scale;
				rect[1] -= trans[1] * scale;
				for(int i = 0; i < 4; i++)
					rect[i] /= scale;
				
				repaint();
			}
			else if(SwingUtilities.isRightMouseButton(e))
			{
				curtrans[0] = -(p1[0] - p2[0]) / scale;
				curtrans[1] = -(p1[1] - p2[1]) / scale;
				repaint();
			}
		}
	}
	public final class CustomMouseWheelListener implements MouseWheelListener {

		@Override
	    public void mouseWheelMoved(MouseWheelEvent e) {
			double d = e.getWheelRotation() / 10.0;
			if(scale + d < 0)
				return;
			
			scale -= d;
			repaint();
	    }
	}
}
