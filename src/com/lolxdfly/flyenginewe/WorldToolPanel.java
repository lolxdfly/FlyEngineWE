package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

public final class WorldToolPanel extends JPanel{
	
	public WorldToolPanel()
	{
		setLayout(new FlowLayout(FlowLayout.LEFT));

		/*****************Edit TileTexture*****************/
		final JButton bETT = new JButton("Edit TileTexture");
		bETT.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  if(MainFrame.toolpanel != null)
        		  MainFrame.mainframe.remove(MainFrame.toolpanel);
        	  if(MainFrame.worldpanel != null)
        		  MainFrame.mainframe.remove(MainFrame.worldpanel);
	        	
        	  MainFrame.worldtilepanel = new WorldTilePanel(MainFrame.tilepanel.getTileImage());
        	  MainFrame.mainframe.add(MainFrame.worldtilepanel, BorderLayout.CENTER);
        	  
        	  MainFrame.toolpanel = new TileToolPanel();
        	  MainFrame.mainframe.add(MainFrame.toolpanel, BorderLayout.PAGE_START);
        	  MainFrame.mainframe.setVisible(true);
          }
        });
		add(bETT);

		/*****************Add Animation*****************/
		final JButton bAnim = new JButton("Add Animation");
		bAnim.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  new AddAnimDialog();
          }
        });
		add(bAnim);

		/*****************Save World*****************/
		final JButton bSave = new JButton("Save World");
		bSave.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  final JFileChooser fc = new JFileChooser();
        	  if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
        	  {
        		  File f = fc.getSelectedFile();
        		  if(!f.getName().endsWith(".wld"))
        			  f = new File(f + ".wld");
        		  MainFrame.worldpanel.save(f);        		  
        	  }
          }
        });
		add(bSave);
		
		setVisible(true);
	}
}
