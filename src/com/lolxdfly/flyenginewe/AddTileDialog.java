package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class AddTileDialog extends JDialog{

	public AddTileDialog() {
		//init
        setTitle("Add Tile");
        setModal(true);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(2, 1));
        
        final JPanel pInput = new JPanel();
        pInput.setLayout(new GridLayout(1, 2));
        
        final JLabel lName = new JLabel("Name: ");
        pInput.add(lName);
        
        //name
        final JTextField tName = new JTextField();
        tName.requestFocusInWindow();
        pInput.add(tName);
        
        add(pInput);
        
        //finish
        final JButton bFinish = new JButton("Finish");
        bFinish.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  if(tName.getText().isEmpty())
        		  return;
        	  
        	  final Tile t = new Tile();
        	  t.name = tName.getText();
        	  t.tileimage = MainFrame.worldtilepanel.tile.getSubimage((int)MainFrame.worldtilepanel.rect[0], (int)MainFrame.worldtilepanel.rect[1], (int)MainFrame.worldtilepanel.rect[2], (int)MainFrame.worldtilepanel.rect[3]);
        	  t.uv_pos[0] = MainFrame.worldtilepanel.rect[0];
        	  t.uv_pos[1] = MainFrame.worldtilepanel.rect[1];
        	  t.uv_pos[2] = MainFrame.worldtilepanel.rect[0] + MainFrame.worldtilepanel.rect[2];
        	  t.uv_pos[3] = MainFrame.worldtilepanel.rect[1] + MainFrame.worldtilepanel.rect[3];
        	  //t.uv_pos[4] = 0;
        	  //t.uv_pos[5] = 0;
        	  t.uv_pos[6] = MainFrame.worldtilepanel.rect[2]; //default width
        	  t.uv_pos[7] = MainFrame.worldtilepanel.rect[3]; //default height

        	  MainFrame.tilepanel.add(t);
        	  
        	  dispose();
          }
        });
        add(bFinish);
        getRootPane().setDefaultButton(bFinish);
        
        pack();
        setVisible(true);
	}
}
