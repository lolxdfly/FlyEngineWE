package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class EditTileDialog extends JDialog{

	public EditTileDialog(final Tile t) {
		//init
        setTitle("Edit Tile");
        setModal(true);
        setLocationRelativeTo(null); 
        setLayout(new GridLayout(3, 1));
        
        final JPanel pInput = new JPanel();
        pInput.setLayout(new GridLayout(1, 2));
        
        final JLabel lName = new JLabel("Name: ");
        pInput.add(lName);
        
        //name
        final JTextField tName = new JTextField();
        tName.requestFocusInWindow();
        tName.setText(t.name);
        pInput.add(tName);
        
        add(pInput);
        
        //finish
        final JButton bFinish = new JButton("Finish");
        bFinish.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  if(tName.getText().isEmpty())
        		  return;
        	  
        	  t.name = tName.getText();
        	          	  
        	  dispose();
          }
        });
        add(bFinish);
        getRootPane().setDefaultButton(bFinish);
        
        //finish
        final JButton bDelete = new JButton("Delete");
        bDelete.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  MainFrame.tilepanel.remove(t);
        	  dispose();
          }
        });
        add(bDelete);
        
        pack();
        setVisible(true);
	}
}
