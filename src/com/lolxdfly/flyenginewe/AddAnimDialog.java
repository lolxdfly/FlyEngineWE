package com.lolxdfly.flyenginewe;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public final class AddAnimDialog  extends JDialog{

	public AddAnimDialog() {
		//init
        setTitle("Add Animation");
        setModal(true);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(2, 1));
        
        final JPanel pInput = new JPanel();
        pInput.setLayout(new GridLayout(4, 2));
        
        final JLabel lRID = new JLabel("RID: ");
        pInput.add(lRID);
        
        //RID
        final JTextField tRID = new JTextField();
        tRID.requestFocusInWindow();
        tRID.setToolTipText("Android Resource Identifier");
        pInput.add(tRID);

        final JLabel lAnimWidth = new JLabel("AnimTexure Width: ");
        pInput.add(lAnimWidth);
        
        //Anim Width
        final JTextField tAnimWidth = new JTextField();
        pInput.add(tAnimWidth);

        final JLabel lAnimHeight = new JLabel("AnimTexure Height: ");
        pInput.add(lAnimHeight);
        
        //Anim Height
        final JTextField tAnimHeight = new JTextField();
        pInput.add(tAnimHeight);
        
        final JLabel ldTime = new JLabel("AnimFPS: ");
        pInput.add(ldTime);
        
        //Anim Height
        final JTextField tdTime = new JTextField();
        pInput.add(tdTime);
        
        add(pInput);        

        final JPanel pOther = new JPanel();
        pOther.setLayout(new GridLayout(4, 1));
        
        //ObjFlags
        final JCheckBox cbA = new JCheckBox("A");
        pOther.add(cbA);
        final JCheckBox cbB = new JCheckBox("B");
        pOther.add(cbB);
        final JCheckBox cbC = new JCheckBox("C");
        pOther.add(cbC);
                
        //finish
        final JButton bFinish = new JButton("Finish");
        bFinish.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  if(tRID.getText().isEmpty() ||
        			  tAnimWidth.getText().isEmpty() ||
        			  tAnimHeight.getText().isEmpty() ||
        			  tdTime.getText().isEmpty()
        			  )
        		 return;
        	  
        	  final Animation anim = new Animation();
        	  anim.RID = tRID.getText();
        	  anim.width = Double.parseDouble(tAnimWidth.getText());
        	  anim.height = Double.parseDouble(tAnimHeight.getText());
        	  anim.dtime = Integer.parseInt(tdTime.getText());
        	  anim.pos[0] = 0;
        	  anim.pos[1] = 0;
        	  anim.pos[2] = 100;
        	  anim.pos[3] = 100;
        	  anim.dwObjFlags = 0;
        	  if(cbA.isSelected())
        		  anim.dwObjFlags |= WorldPanel.A;
        	  if(cbB.isSelected())
        		  anim.dwObjFlags |= WorldPanel.B;
        	  if(cbC.isSelected())
        		  anim.dwObjFlags |= WorldPanel.C;
        	  
        	  MainFrame.worldpanel.AddTile(anim);
        	  
        	  dispose();
          }
        });
        pOther.add(bFinish);
        getRootPane().setDefaultButton(bFinish);
        
        add(pOther);
        
        pack();
        setVisible(true);
	}
}
