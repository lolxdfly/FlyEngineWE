package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public final class WorldPanel extends JPanel  {
	//todo
	public final static int A 	= 1 << 0;
	public final static int	B 	= 1 << 1;
	public final static int C	= 1 << 2;
	
	private final ArrayList<Tile> mTiles;
	private final ArrayList<Animation> mAnims;
	private final double p1[] = new double[2], p2[] = new double[2];
	private double scale = 1.0;
	private final double trans[] = new double[2];
	private final double curtrans[] = new double[2];

	public static Tile drag;
	private static int dragpos[] = new int[2];
	
	private Tile selected;
	private short movetype = 0;
	
	public WorldPanel()
	{
		mTiles = new ArrayList<Tile>();
		mAnims = new ArrayList<Animation>();
		setSize(1920, 1080); //engine res
		addMouseListener(new CustomMouseListener());
		addMouseMotionListener(new CustomMouseMotionListener());
		addMouseWheelListener(new CustomMouseWheelListener());
		setVisible(true);
	}

	public final void AddTile(final Animation a)
	{
		mAnims.add(a);
		repaint();
	}
	public final void RemoveTile(final Animation a)
	{
		mAnims.remove(a);
		selected = null;
		repaint();
	}
	public final void AddTile(final Tile t)
	{
		final Tile add = new Tile(t);
		//translate/scale back
		add.uv_pos[4] -= trans[0] * scale;
		add.uv_pos[5] -= trans[1] * scale;
		add.uv_pos[4] /= scale;
		add.uv_pos[5] /= scale;
		mTiles.add(add);
	}
	public final void RemoveTile(final Tile t)
	{
		mTiles.remove(t);
		selected = null;
		repaint();
	}
		
	public final void setDragPos(final int x, final int y)
	{
		dragpos[0] = x;
		dragpos[1] = y;
		dragpos[0] -= trans[0] * scale;
		dragpos[1] -= trans[1] * scale;
		dragpos[0] /= scale;
		dragpos[1] /= scale;
		repaint();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	    final Graphics2D g2d = (Graphics2D) g;
	    g2d.scale(scale, scale);
	    g2d.translate(trans[0] + curtrans[0], trans[1] + curtrans[1]);
	    
	    //render tile
	    for(final Tile tile : mTiles)
	    	g2d.drawImage(tile.tileimage, (int)tile.uv_pos[4], (int)tile.uv_pos[5], (int)tile.uv_pos[6], (int)tile.uv_pos[7], this);
	    
	    //render Anim
	    for(final Animation anim : mAnims)
	    {
	    	g2d.drawRect((int)anim.pos[0], (int)anim.pos[1], (int)(anim.pos[2] - anim.pos[0]), (int)(anim.pos[3] - anim.pos[1]));
	    	g2d.drawChars(anim.RID.toCharArray(), 0, anim.RID.length(), (int)(anim.pos[0] + ((anim.pos[2] - anim.pos[0]) / 2.0)), (int)(anim.pos[1] + ((anim.pos[3] - anim.pos[1]) / 2.0)));
	    }
		
	    //render selected
	    if(selected != null)
	    {
	    	final Stroke oldStroke = g2d.getStroke();
	    	g2d.setStroke(new BasicStroke(2));
	    	g2d.drawRect((int)selected.uv_pos[4], (int)selected.uv_pos[5], (int)selected.uv_pos[6], (int)selected.uv_pos[7]);
	    	g2d.drawOval((int)selected.uv_pos[4] - 5, (int)selected.uv_pos[5] - 5, 10, 10);
	    	g2d.drawOval((int)selected.uv_pos[4] + (int)selected.uv_pos[6] - 5, (int)selected.uv_pos[5] - 5, 10, 10);
	    	g2d.drawOval((int)selected.uv_pos[4] - 5, (int)selected.uv_pos[5] + (int)selected.uv_pos[7] - 5, 10, 10);
	    	g2d.drawOval((int)selected.uv_pos[4] + (int)selected.uv_pos[6] - 5, (int)selected.uv_pos[5] + (int)selected.uv_pos[7] - 5, 10, 10);
	    	g2d.setStroke(oldStroke);
	    }
	    
	    //render drag
	    if(drag != null)
		{
			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
			g2d.drawImage(drag.tileimage, dragpos[0], dragpos[1], this);
		}
	    
	}
	
	private static final boolean contains(double px, double py, double x, double y, double xw, double yw)
	{
		return(x < px && px < x + xw &&
				y < py && py < y + yw);		
	}
	
	private final class CustomMouseListener extends MouseAdapter {		
		@Override
		public void mousePressed(MouseEvent e) {
			p1[0] = e.getX();
			p1[1] = e.getY();
			
			if(SwingUtilities.isLeftMouseButton(e))
			{					
				//translate/scale back
				p1[0] -= trans[0] * scale;
				p1[1] -= trans[1] * scale;
				p1[0] /= scale;
				p1[1] /= scale;
				
				//select
				Tile newselected = null;
				for(int i = 0; i < mTiles.size(); i++)
				{
					final Tile tile = mTiles.get(i);
					if(contains(p1[0], p1[1], tile.uv_pos[4] - 5, tile.uv_pos[5] - 5, tile.uv_pos[6] + 10, tile.uv_pos[7] + 10))
						newselected = tile;
				}
				if(newselected == null)
				{
					selected = null;
					repaint();
					return;					
				}
				else if(selected != newselected)
				{
					selected = newselected;
					repaint();
					return;
				}
				
				//edit/del tile
				if(e.getClickCount() == 2)
					new EditWldElemDialog(selected);
				else //scale/move
				{
					if(contains(p1[0], p1[1], selected.uv_pos[4] - 5, selected.uv_pos[5] - 5, 10, 10))
					{
						//move tl
						movetype = 1;
					}
					else if(contains(p1[0], p1[1], selected.uv_pos[4] + selected.uv_pos[6] - 5, selected.uv_pos[5] - 5, 10, 10))
					{
						//move tr
						movetype = 2;
					}
					else if(contains(p1[0], p1[1], selected.uv_pos[4] - 5, selected.uv_pos[5] + selected.uv_pos[7] - 5, 10, 10))
					{
						//move bl
						movetype = 3;
					}
					else if(contains(p1[0], p1[1], selected.uv_pos[4] + selected.uv_pos[6] - 5, selected.uv_pos[5] + selected.uv_pos[7] - 5, 10, 10))
					{
						//move br
						movetype = 4;
					}
					else
					{
						//move tile
						movetype = 5;
					}
				}
			}
		}
		@Override
		public void mouseReleased(MouseEvent e) {
	        trans[0] += curtrans[0];
	        trans[1] += curtrans[1];
	        curtrans[0] = 0;
	        curtrans[1] = 0;  
			repaint();
		}
	}
	private final class CustomMouseMotionListener extends MouseMotionAdapter {

		@Override
		public void mouseDragged(MouseEvent e) {			
			p2[0] = e.getX();
			p2[1] = e.getY();
			
			if(SwingUtilities.isLeftMouseButton(e) && selected != null)
			{
				//translate/scale back
				p2[0] -= trans[0] * scale;
				p2[1] -= trans[1] * scale;
				p2[0] /= scale;
				p2[1] /= scale;
				
				switch(movetype)
				{
					case 1: //tl
					{
						if(selected.uv_pos[4] + selected.uv_pos[6] - p2[0] > 1 && selected.uv_pos[5] + selected.uv_pos[7] - p2[1] > 1)
						{
							selected.uv_pos[6] += (selected.uv_pos[4] - p2[0]);
							selected.uv_pos[7] += (selected.uv_pos[5] - p2[1]);
							selected.uv_pos[4] = p2[0];
							selected.uv_pos[5] = p2[1];
						}
						break;
					}
					case 2: //tr
					{
						if(p2[0] - selected.uv_pos[4] > 1 && selected.uv_pos[7] + selected.uv_pos[5] - p2[1] > 1)
						{
							selected.uv_pos[6] = (p2[0] - selected.uv_pos[4]);
							selected.uv_pos[7] += (selected.uv_pos[5] - p2[1]);
							selected.uv_pos[5] = p2[1];
						}
						break;
					}
					case 3: //bl
					{
						if(p2[1] - selected.uv_pos[5] > 1 && selected.uv_pos[6] + selected.uv_pos[4] - p2[0] > 1)
						{
							selected.uv_pos[6] += (selected.uv_pos[4] - p2[0]);
							selected.uv_pos[7] = (p2[1] - selected.uv_pos[5]);
							selected.uv_pos[4] = p2[0];
						}
						break;
					}
					case 4: //br
					{
						if(p2[0] - selected.uv_pos[4] > 1 && p2[1] - selected.uv_pos[5] > 1)
						{
							selected.uv_pos[6] = (p2[0] - selected.uv_pos[4]);
							selected.uv_pos[7] = (p2[1] - selected.uv_pos[5]);
						}
						break;
					}
					case 5: //move
					{
						selected.uv_pos[4] = p2[0] - (selected.uv_pos[6] / 2.0);
						selected.uv_pos[5] = p2[1] - (selected.uv_pos[7] / 2.0);
						break;
					}
				}
				repaint();
			}
			else if(SwingUtilities.isRightMouseButton(e))
			{
				curtrans[0] = -(p1[0] - p2[0]) / scale;
				curtrans[1] = -(p1[1] - p2[1]) / scale;
				repaint();
			}
		}
	}
	public final class CustomMouseWheelListener implements MouseWheelListener {

		@Override
	    public void mouseWheelMoved(MouseWheelEvent e) {
			double d = e.getWheelRotation() / 10.0;
			if(scale - d < 0)
				return;
			
			scale -= d;
			repaint();
	    }
	}
	
	public final void load(final File f)
	{
		try(final Scanner s = new Scanner(f).useLocale(Locale.US))
		{
			while(s.hasNextLine() && s.hasNext())
			{
				if(!s.nextBoolean())
				{
					final Tile tile = new Tile();
					for(int i = 0; i < 8; i++)
						tile.uv_pos[i] = s.nextDouble();
					tile.uv_pos[6] -= tile.uv_pos[4];
					tile.uv_pos[7] -= tile.uv_pos[5];
					tile.tileimage = MainFrame.tilepanel.getTileImage().getSubimage((int)tile.uv_pos[0], (int)tile.uv_pos[1], (int)tile.uv_pos[6], (int)tile.uv_pos[7]);
					tile.dwObjFlags = s.nextInt();
					mTiles.add(tile);
				}
				else
				{
					final Animation anim = new Animation();
					anim.RID = s.next();
					anim.width = s.nextDouble();
					anim.height = s.nextDouble();
					anim.dtime = s.nextInt();
					for(int i = 0; i < 4; i++)
						anim.pos[i] = s.nextDouble();
					anim.dwObjFlags = s.nextInt();
					mAnims.add(anim);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public final void save(final File f)
	{
		try(final BufferedWriter bw = new BufferedWriter(new FileWriter(f)))
		{
			for(final Tile tile : mTiles)
				bw.write("0 " + tile.uv_pos[0] + " " + tile.uv_pos[1] + " " + tile.uv_pos[2] + " " + tile.uv_pos[3] + " " + tile.uv_pos[4] + " " + tile.uv_pos[5] + " " + (tile.uv_pos[4] + tile.uv_pos[6]) + " " + (tile.uv_pos[5] + tile.uv_pos[7]) + " " + tile.dwObjFlags + "\r\n");
			for(final Animation anim : mAnims)
				bw.write("1 " + anim.RID + " " + anim.width + " " + anim.height + " " + anim.dtime + " " + anim.pos[0] + " " + anim.pos[1] + " " + anim.pos[2] + " " + anim.pos[3] + " " + anim.dwObjFlags + "\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
