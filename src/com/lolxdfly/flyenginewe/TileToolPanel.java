package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

public final class TileToolPanel extends JPanel{
	
	public TileToolPanel()
	{
		setLayout(new FlowLayout(FlowLayout.LEFT));

		/*****************Create new World*****************/
		final JButton bCW = new JButton("Create new World");
		bCW.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  if(MainFrame.toolpanel != null)
        		  MainFrame.mainframe.remove(MainFrame.toolpanel);
        	  if(MainFrame.worldtilepanel != null)
        		  MainFrame.mainframe.remove(MainFrame.worldtilepanel);
	        	
        	  MainFrame.worldpanel = new WorldPanel();
        	  MainFrame.mainframe.add(MainFrame.worldpanel, BorderLayout.CENTER);
        	  
        	  MainFrame.toolpanel = new WorldToolPanel();
        	  MainFrame.mainframe.add(MainFrame.toolpanel, BorderLayout.PAGE_START);
        	  MainFrame.mainframe.setVisible(true);
          }
        });
		add(bCW);
		

		if(MainFrame.worldpanel != null)
		{
			/*****************Edit World*****************/
			final JButton bBtW = new JButton("Edit World");
			bBtW.addActionListener(new ActionListener()
	        {
	          public void actionPerformed(ActionEvent e)
	          {
	        	  if(MainFrame.toolpanel != null)
	        		  MainFrame.mainframe.remove(MainFrame.toolpanel);
	        	  if(MainFrame.worldtilepanel != null)
	        		  MainFrame.mainframe.remove(MainFrame.worldtilepanel);
		      
	        	  MainFrame.mainframe.add(MainFrame.worldpanel, BorderLayout.CENTER);
	        	  
	        	  MainFrame.toolpanel = new WorldToolPanel();
	        	  MainFrame.mainframe.add(MainFrame.toolpanel, BorderLayout.PAGE_START);
	        	  MainFrame.mainframe.setVisible(true);
	          }
	        });
			add(bBtW);
		}

		/*****************Update TileTexture*****************/
		final JButton bCTT = new JButton("Update TileTexture");
		bCTT.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
				final JFileChooser fc = new JFileChooser();
				fc.setFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
		        if(fc.showDialog(null, "Open TileTexture") == JFileChooser.APPROVE_OPTION)
		        {
		        	if(MainFrame.worldtilepanel != null)
		        		MainFrame.mainframe.remove(MainFrame.worldtilepanel);
		        			        	
		        	//open worldtilepanel
		        	BufferedImage tileimg = null;
					try {
						tileimg = ImageIO.read(fc.getSelectedFile());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
		        	
					MainFrame.worldtilepanel = new WorldTilePanel(tileimg);
					MainFrame.mainframe.add(MainFrame.worldtilepanel, BorderLayout.CENTER);
		        	
		        	//set tile image
					MainFrame.tilepanel.setTileImage(tileimg);
		        }
          }
        });
		add(bCTT);

		/*****************Save TileSet*****************/
		final JButton bFinish = new JButton("Save TileSet");
        bFinish.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  final JFileChooser fc = new JFileChooser();
        	  if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
        	  {
        		  File f = fc.getSelectedFile();
        		  if(!f.getName().endsWith(".tileset"))
        			  f = new File(f + ".tileset");
        		  MainFrame.tilepanel.save(f);        		  
        	  }
          }
        });
		add(bFinish);
		
		setVisible(true);
	}
}
