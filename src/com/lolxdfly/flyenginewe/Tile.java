package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.image.BufferedImage;
import java.io.Serializable;

//bAnim(false) uv_pos[0] uv_pos[1] uv_pos[2] uv_pos[3] uv_pos[4] uv_pos[5] uv_pos[6] uv_pos[7] dwObjFlags
public final class Tile implements Serializable{

	//visual representation
	public String name = "unnamed";
	public transient BufferedImage tileimage = null;
	
	//engine-related
	final double uv_pos[] = new double[8];
	public int dwObjFlags = 0;
	
	public Tile()
	{	}
	
	public Tile(final Tile t)
	{
		name = t.name;
		tileimage = t.tileimage;
		System.arraycopy(t.uv_pos, 0, uv_pos, 0, 8);
		dwObjFlags = t.dwObjFlags;
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		tileimage.flush();
		tileimage = null;	    	
	}
	
	@Override
	public String toString() {
		return name;
	}
}
