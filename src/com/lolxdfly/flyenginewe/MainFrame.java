package com.lolxdfly.flyenginewe;
/* Copyright (C) GiantLogic Entertainment - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by lolxdfly <lolxdfly@gmail.com>, January 2016
 */

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame {

	public static MainFrame mainframe;
	public static TilePanel tilepanel;
	public static WorldTilePanel worldtilepanel;
	public static WorldPanel worldpanel;
	public static JPanel toolpanel;
	
	public static void main(String[] args) {
		mainframe = new MainFrame();
	}
	
	public MainFrame()
	{
		//init
		setTitle("FlyEngine - WorldEditor");
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//init layout
		setLayout(new BorderLayout());
		
		//init Menu
		final Menu pMenu = new Menu();
		setJMenuBar(pMenu);
		
		//make all visible
		setVisible(true);
	}
}
